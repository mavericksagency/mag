<?php get_header();
the_post();
get_template_part('partials/title');
?>
<section class="section section-content section-negative-margin">
	<div class="wrap">
		<?php if(get_field('add_header_page_custom')){ 
			$header = get_field('header_block');
			?>
		<div class="row padding-side-xl-15">
			<div class="col col-md-5 text-center">
				<img src="<?= $header['image']; ?>" alt="">
			</div>
			<div class="col col-md-7">
				<div class="content">
					<h2>
						<?= $header['subtitle']; ?>
					</h2>
					<p>
						<?= $header['description']; ?>
					</p>
				</div>
			</div>
		</div>
		<?php } ?>
		<div class="content">
			<div class="wrap-sm">
				<?php the_content(); ?>
			</div>
		</div>
		<?php get_template_part('partials/constructor_part/main'); ?>
	</div>
</section>
<?php get_footer();