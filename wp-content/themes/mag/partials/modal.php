
<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="modalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalCenterTitle">#</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            <svg class="icon">
              <use xlink:href="#close">
            </svg>
          </span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row clearfix">
          <div class="col col-md-9 content">#</div>
          <div class="col col-md-3">
            <div class="help-block">
              <span class="help-title">
                Need a quote?
              </span>
              <a href="tel:" class="phone">
                <span>Call us now</span>
                <span class="number">#</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>