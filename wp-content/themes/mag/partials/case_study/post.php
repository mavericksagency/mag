<?php 
    global $post;
    $url = get_permalink();
?>
<div class="col col-sm-6 col-md-4">
    <div class="case-panel">
        <?php 
            if(has_post_thumbnail($post->ID)){
                echo '<a href="'.$url.'" class="case-panel-img">
                            <img src="'.get_the_post_thumbnail_url().'" alt="" class="cover-img">
                        </a>';
            }
        ?>
        <div class="case-panel-title">
            <a href="<?= $url; ?>" class="case-title"><?php the_title(); ?></a>
            <?php 
                $location = get_field('location');
                if($location){
            ?>
            <span class="case-location">
                <?= $location; ?>
            </span>
            <?php } ?>
        </div>
        <a href="<?= $url; ?>" class="btn case-link">
            View case study
        </a>
    </div>
</div>