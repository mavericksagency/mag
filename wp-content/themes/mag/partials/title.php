<?php global $post, $term_page; ?>
<div class="title-block">
	<div class="wrap">
		<h1>
			<?php 
				if($post->post_type == 'post'){
					echo 'NEWS';
				}else if(!is_archive()){ 
					echo $term_page->name ?  $term_page->name : $post->post_title; 
				}else{ 
					the_archive_title(); 
				} 
			?>
		</h1>
	</div>
</div>