<?php 
    global $post, $section; 
    if(!empty($section['sliders']) && count($section['sliders']) > 0){
?>
<div class="main-slider anim-block transformY-top" id="main-slider">
    <?php
        foreach($section['sliders'] as $item){
            ?>
            <div class="main-slider-item">
                <img src="<?= $item['image']; ?>" alt="" class="cover-img">
                <div class="slider-content">
                    <span class="upper-slider-text">
                        <?= $item['title']; ?>
                    </span>
                    <h2 class="main-slider-title">
                        <?= $item['subtitle']; ?>
                    </h2>
                    <span class="main-slider-desc">
                        <?= $item['description']; ?>
                    </span>
                </div>
            </div>
            <?php
        }
    ?>
</div>
<?php }