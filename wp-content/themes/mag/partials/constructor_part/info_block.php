<?php 
    global $post, $section; 
    $tag = $section['use_h1'] ? 'h1' : 'h2';
?>
<div class="main-section-info anim-block transformY-top">
    <div class="wrap">
        <div class="row">
            <div class="col col-md-5">
                <<?= $tag; ?> class="main-title">
                    <?= $section['title']; ?>
                </<?= $tag; ?>>
            </div>
            <div class="col col-md-7">
                <?php if(isset($section['subtitle']) && !empty($section['title'])){ ?>
                <h3>
                    <?= $section['subtitle']; ?>
                </h3>
                <?php } ?>
                <?php if(isset($section['content']) && !empty($section['content'])){ 
                    echo wpautop($section['content']);
                } ?>
                
                <a href="<?= $section['button']['url'] ?>" class="btn btn-sm-padding" <?= $section['button']['target']=='_blank'?'target="_blank"':''; ?>>
                    <?=  $section['button']['title'] ?>
                </a>
            </div>
        </div>
    </div>
</div>