<?php 
	$client_content = get_field('content_client', 'option');
?>
<section class="section section-carousel anim-block transformY-top">
	<div class="wrap">
		<?php
			if($client_content['title']){
				echo '<h2>'.$client_content['title'].'</h2>';
			}
			if($client_content['title']){
				echo '<div class="section-desc">'.$client_content['description'].'</div>';
			}
			if(($client_content['clients_logo']) > 0){
		?>
		<div class="carousel" id="carousel">
			<?php 
				foreach($client_content['clients_logo'] as $logo){
					echo '<div class="carousel-item"><div class="img"><img src="'.$logo['image'].'" alt=""></div></div>';
				}
			?>
		</div>
		<?php } ?>
	</div>
</section>