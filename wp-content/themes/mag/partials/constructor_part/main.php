<?php 
    global $post, $section;
    $constructor = get_field('constructor');
    if($constructor){
        foreach($constructor as $section){
            get_template_part('partials/constructor_part/'.$section['acf_fc_layout']);
        }
    }
?>