<?php 
	global $post;
	$services_block = get_field('content_services_block', 'option');
?>
<section class="section anim-block transformY-top" >
	<div class="row margin-side-none align-items-stretch">
		<div class="col col-lg-5">
			<div class="service-left">
				<img src="<?= $services_block['image']; ?>" alt="">
				<div class="service-inner service-left-inner">
					<h3><?= $services_block['title']; ?></h3>
					<p>
						<?= $services_block['description']; ?>
					</p>
					<a href="<?= $services_block['button']['url'] ?>" class="btn service-link" <?= $services_block['button']['target']=='_blank'?'target="_blank"':''; ?>>
						<?=  $services_block['button']['title'] ?>
					</a>
				</div>
			</div>
		</div>
		<div class="col col-lg-7">
			<div class="service-right">
				<div class="service-inner service-right-inner">
					<h2>OUR SERVICES</h2>
					<div class="service-list">
						<div class="row margin-side-none">
							<?php 
								if(count($services_block['services'])){
									foreach($services_block['services'] as $post_id){
										$post = get_post($post_id);
										?>
										<div class="col col-md-6 col-6">
											<div class="service-item">
												<h3>
													<?= $post->post_title; ?>
												</h3>
												<p>
													<?= wp_trim_words($post->post_content, 25); ?>
												</p>
												<a href="<?= get_permalink($post_id); ?>" class="btn service-link dark">Find out more</a>
											</div>
										</div>
										<?php
										wp_reset_postdata();
									}
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>