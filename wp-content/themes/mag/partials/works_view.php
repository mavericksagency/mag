<?php 
    global $post, $section, $posts; 
    if(!empty($posts) && count($posts) > 0){
?>
<section class="section section-work anim-block transformY-top">
	<div class="wrap">
		<h2>
			View our work
		</h2>
		<div class="row">
			<?php foreach($posts as $post){
				get_template_part('partials/case_study/post');
			} ?>
		</div>
	</div>
</section>
<?php } ?>