<?php 
    global $post;
    $url = get_permalink();
?>
<div class="news-item">
    <?php 
        if(has_post_thumbnail($post->ID)){
            echo '<img src="'.get_the_post_thumbnail_url().'" alt=""/>';
        }
    ?>
    <div class="news-item-inner">
        <h2>
            <a href="<?= $url; ?>">
                <?= the_title(); ?>
            </a>
        </h2>
        <span class="news-date">
            <?= get_the_date('j F Y', $post); ?>
        </span>
        <div class="news-description">
            <?php the_excerpt(); ?>
        </div>
        <a href="<?= $url; ?>" class="btn-more">
            Read more 
        </a>
    </div>
</div>