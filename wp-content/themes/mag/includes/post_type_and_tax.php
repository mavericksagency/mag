<?php

function register_post_types(){
    register_post_type('case_studies', array(
        'label'  => null,
        'labels' => array(
            'name'               => 'Case study',
            'singular_name'      => 'Case study',
            'add_new'            => 'Add',
            'add_new_item'       => 'Add',
            'edit_item'          => 'Edit',
            'new_item'           => 'Add',
            'view_item'          => 'View',
            'search_items'       => 'Search',
            'not_found'          => 'Not found',
            'not_found_in_trash' => 'Not found',
            'parent_item_colon'  => '',
            'menu_name'          => 'Case study'
        ),
        'has_archive'         => true,
        'public'              => true,
        'exclude_from_search' => true,
        'show_in_menu'        => true,
        'menu_position'       => null,
        'menu_icon'           => 'dashicons-media-text', 
        'supports'            => array('title', 'editor', 'thumbnail'),
    ));
    register_post_type('services', array(
        'label'  => null,
        'labels' => array(
            'name'               => 'Services',
            'singular_name'      => 'Services',
            'add_new'            => 'Add',
            'add_new_item'       => 'Add',
            'edit_item'          => 'Edit',
            'new_item'           => 'Add',
            'view_item'          => 'View',
            'search_items'       => 'Search',
            'not_found'          => 'Not found',
            'not_found_in_trash' => 'Not found',
            'parent_item_colon'  => '',
            'menu_name'          => 'Services'
        ),
        'has_archive'         => true,
        'public'              => true,
        'exclude_from_search' => true,
        'show_in_menu'        => true,
        'menu_position'       => null,
        'menu_icon'           => 'dashicons-admin-site', 
        'supports'            => array('title', 'thumbnail', 'editor'),
    ));
}