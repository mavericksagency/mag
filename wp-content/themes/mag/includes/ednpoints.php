<?php

/**
 * load more post
 */
function loadMorePost(){

    $args = array(
        'post_type'         => 'post',
        'posts_per_page'    => 6,
        'order'             => 'DESC',
        'paged'             => $_POST['query']['page']
    );

    $query = new WP_Query($args);

    ob_start();

    while($query->have_posts()){
        $query->the_post();
        get_template_part('partials/post/small');
    }
    
    $data = ob_get_clean();

    $response = array(
        'status'        => 'done',
        'content'       => $data
    );

    wp_send_json($response);
}
add_action('wp_ajax_load_more_post', 'loadMorePost');
add_action('wp_ajax_nopriv_load_more_post', 'loadMorePost');


/**
 * contact form
 */
function contactForm(){

    if (empty($_POST) || ! wp_verify_nonce( $_POST['nonce'], 'nonce_printspace') ){
        throw new Exception('Wrong data!');
    }else{
        $headers = 'From: '.get_bloginfo('name').' <no-reply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n" .
        'Reply-To: '.get_bloginfo('name').' <no-reply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n" ."Content-type: text/html; charset=utf-8 \r\n";;

        $subject = "New message from site.";

        $message = '<p>
                        Name : '.$_POST['name'].'<br/>
                        email : '.$_POST['email'].'<br/>
                        phone : '.$_POST['phone'].'<br/>
                        Text : '.$_POST['message'].'<br/>
                    </p>';

        $mailto = get_field('email_for_message', 'option');

        $response['intercome'] = addtoIntercome(array(
            'email'     => $_POST['email'],
            'name'      => $_POST['name'],
            'phone'     => $_POST['phone'],
            // 'message'   => $_POST['message']
        ), $_POST['message']);

        if(wp_mail($mailto, $subject, $message, $headers)){
            $response['status'] = 'done';
        }else{
            $response['status'] = 'error';
        }
    }
    
    wp_send_json($response);
}

add_action('wp_ajax_add_message', 'contactForm');
add_action('wp_ajax_nopriv_add_message', 'contactForm');


function addtoIntercome($data, $message = 'Empty message from user'){

    //$access_token = "dG9rOmUwNWU4OWJiX2JhNGNfNDI1NV84ZDAxXzZkMjdhYTVjYzAwYjoxOjA=";
    $access_token = get_field('intercom', 'option');
    $endpoint = "https://api.intercom.io/contacts";
    $endpointMessage = "https://api.intercom.io/messages";
    $params = json_encode($data);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $endpoint, 
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "$params", 
        CURLOPT_HTTPHEADER => array(
            "accept: application/json",
            "authorization: Bearer $access_token",
            "cache-control: no-cache",
            "content-type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    if(!$err){
        $dataResponse = json_decode($response, true);
        $dataMessage = array(
            'from'  => array(
                "type"  => "contact",
                'id'    =>  $dataResponse['id']
            ),
            "body" => $message
        );

        $curlMess = curl_init();

        $paramsMess = json_encode($dataMessage);

        curl_setopt_array($curlMess, array(
            CURLOPT_URL => $endpointMessage, 
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "$paramsMess", 
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "authorization: Bearer $access_token",
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));

        $responseMess = curl_exec($curlMess);
        $errMess = curl_error($curlMess);
    }

    return $err ? $err : true;

}