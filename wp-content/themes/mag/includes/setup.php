<?php 

function setup_theme(){
    //add thumbnails
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');

    register_nav_menu('primary', 'Main Menu', 'mag');
    register_nav_menu('footer_col_1', 'Get to know us', 'mag');
    register_nav_menu('footer_col_2', 'What we do', 'mag');

    //register option page
    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page(array(
            'page_title' 	=> 'Theme settings',
            'menu_title'	=> 'Theme settings',
            'menu_slug' 	=> 'theme-general-settings',
            'capability'	=> 'edit_posts',
            'redirect'      => false
        ));
    }
}