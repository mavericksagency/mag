<?php

function theme_styles_and_scripts(){

    //style
    wp_register_style('Lato', 'https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i');
    wp_register_style('normalize', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css');
    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_register_style('slick', get_template_directory_uri() . '/css/slick.css');
    wp_register_style('styles', get_template_directory_uri() . '/css/styles.css');

    wp_enqueue_style('Lato');
    wp_enqueue_style('normalize');
    wp_enqueue_style('bootstrap');
    wp_enqueue_style('slick');
    wp_enqueue_style('styles');

    //scripts
    wp_deregister_script('jquery');

    wp_register_script('jquery', "https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js", array(), '3.4.0', false);
    wp_register_script('bootstrap', get_template_directory_uri().'/js/bootstrap.min.js', array(), '1.0', true);
    wp_register_script('slick', get_template_directory_uri().'/js/slick.js', array(), '1.0', true);
    wp_register_script('main', get_template_directory_uri().'/js/main.js', array('jquery', 'slick'), '1.0', true);


    //active scripts
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap');
    wp_enqueue_script('slick');
    wp_enqueue_script('main');

	wp_localize_script('main', 'api_settings', array(
		'root'          => esc_url_raw( rest_url()),
        'nonce'         => wp_create_nonce('wp_rest'),
        'ajax_url'      => site_url().'/wp-admin/admin-ajax.php',
        'template'      => get_bloginfo('template_url').'/'
    ));
}