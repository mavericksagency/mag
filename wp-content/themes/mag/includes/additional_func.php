<?php

//remove dots excerpt
function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


function toMM($val){

    if(strpos($val, 'A') !== false){
        return $val;
    }

    $val = str_replace("\"","",$val);

    $arr = explode(' x ', $val);

    $val = (int)$arr[0] * 25.4 .' x '. (int)$arr[1] * 25.4;

    return $val;
}

// pagination
function pagination($pages = '', $range = 3){  
    $showitems = ($range * 2)+1;  

    global $paged;
    if(empty($paged)){ $paged = 1;}

    if($pages == ''){
       global $query;
       $pages = $query->max_num_pages;
       if(!$pages){
           $pages = 1;
       }
   }   

   if(1 != $pages){
        echo "<ul class=\"pagination\">";

        if($paged != 1){
            echo '<li><a class="prev pagination-arrow" href="'.get_pagenum_link($paged - 1).'">«</a></li>';
        }

        for ($i=1; $i <= $pages; $i++){
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
               echo ($paged == $i) ? "<li><a href=\"".get_pagenum_link($i)."\" class=\"current\">".$i."</span></li>":
                "<li><a href='".get_pagenum_link($i)."' class=\"page-numbers\">".$i."</a></li>";
            }
        }  

        if($paged != $pages){
            echo '<li><a class="next pagination-arrow" href="'.get_pagenum_link($paged + 1).'">»</a></li>';
        }
        
        echo '</ul>';
   }
}


function getPhoneNumber(){
    $phone = get_field('phone_number', 'option');
    if($phone){
        return 	'<a href="tel:'.$phone.'" class="call">
                    Call '.$phone.'
                </a>';
    }
}

function getJsonBlock($block){
    $block['content'] = preg_replace('/(<img.+\/>)/', '<div class="img-block">$1</div>', $block['content']);

    return json_encode($block);
}

function sortStady($posts, $post_id){
    if(count($posts->posts) == 0){
        return [];
    }
    $newPosts = [];
    foreach($posts->posts as $post){
        $ids = get_field('service', $post->ID);
        if(count($ids) > 0 && in_array($post_id, $ids)){
            $newPosts[] = $post;
        }
    }

    return $newPosts;
}
