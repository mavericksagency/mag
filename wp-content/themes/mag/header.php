<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<?php wp_head(); ?>
	<link rel="apple-touch-icon" sizes="57x57" href="<?= get_template_directory_uri() ?>/img/fav/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?= get_template_directory_uri() ?>/img/fav/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= get_template_directory_uri() ?>/img/fav/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= get_template_directory_uri() ?>/img/fav/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= get_template_directory_uri() ?>/img/fav/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= get_template_directory_uri() ?>/img/fav/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?= get_template_directory_uri() ?>/img/fav/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= get_template_directory_uri() ?>/img/fav/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= get_template_directory_uri() ?>/img/fav/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?= get_template_directory_uri() ?>/img/fav/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= get_template_directory_uri() ?>/img/fav/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?= get_template_directory_uri() ?>/img/fav/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= get_template_directory_uri() ?>/img/fav/favicon-16x16.png">
	<link rel="manifest" href="<?= get_template_directory_uri() ?>/img/fav/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?= get_template_directory_uri() ?>/img/fav/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
</head>
<body class="anim">
	<div class="d-none">
		<?php get_template_part('partials/svgmap'); ?>
	</div>
	<header class="header"> 
		<div class="wrap">
			<a href="/" class="logo">
				<img src="<?= get_template_directory_uri() ?>/img/logo.svg" alt="Mag Logo">
			</a>
			<button class="btn-tr btn-menu" id="btn-menu" type="button">
				<svg class="icon menu-icon">
					<use xlink:href="#menu">
				</svg>
				<svg class="icon close-icon">
					<use xlink:href="#close">
				</svg>
			</button>
			<div class="header-row" id="menu-row">
				<nav class="nav">
				<?php 
					wp_nav_menu(array(
						'theme_location'  	=> 'primary',
						'container'			=> false,
						'fallback_cb'		=> false,
						'items_wrap'      	=> '<ul class="main-menu">%3$s</ul>',
						'depth'				=> 2,
						'walker' 			=> new submenuWrap
					));
				?>
				</nav>
				<?= getPhoneNumber(); ?>
			</div>
		</div>
	</header>

	<main class="main" id="main">