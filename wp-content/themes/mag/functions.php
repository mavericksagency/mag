<?php
//const 


//setup

//includes
include(get_template_directory(). '/includes/front/enqueue.php');
include(get_template_directory(). '/includes/ednpoints.php');
include(get_template_directory(). '/includes/setup.php');
include(get_template_directory(). '/includes/additional_func.php');
include(get_template_directory(). '/includes/walker.php');
include(get_template_directory(). '/includes/post_type_and_tax.php');

//hooks

//scripts and style
add_action('wp_enqueue_scripts', 'theme_styles_and_scripts');
//post type and tax
add_action('init', 'register_post_types');
//themes settings
add_action('after_setup_theme', 'setup_theme');
//remove admin panel
add_filter('show_admin_bar', '__return_false');
// turn off xmlrpc
add_filter('xmlrpc_enabled', '__return_false');

