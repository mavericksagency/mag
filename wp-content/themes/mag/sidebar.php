<div class="col col-lg-3 col-xl-4">
    <div class="aside">
        <?php  
            $terms = get_terms(array(
                'taxonomy' => 'category',
                'hide_empty' => false,
            ));
            if(!empty($terms) && count($terms) > 0){
        ?>
        <div class="aside-block">
            <h3 class="aside-title">
                Categories
            </h3>
            <ul class="aside-list text-uppercase">
                <?php 
                    foreach($terms as $term){
                        echo '<li><a href="'.get_term_link($term->term_id, $term->taxonomy).'">'.$term->name.'</a></li>';
                    }
                ?> 
            </ul>
        </div>
        <?php } ?>
        <div class="aside-block">
            <h3 class="aside-title">
                Archive
            </h3>
            <ul class="aside-list">
                <?php wp_get_archives(); ?>
            </ul>
        </div>
    </div>
</div>