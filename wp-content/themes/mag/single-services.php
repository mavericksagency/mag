<?php 
/**
 * Template Name: Sector page
 */
get_header();
the_post();
get_template_part('partials/title');
?>

	<section class="section section-content section-md-margin-bottom-none section-negative-margin anim-block transformY-top">
		<div class="wrap">
			<div class="row padding-side-xl-15">
				<div class="col col-md-5 text-center">
					<img src="<?= get_the_post_thumbnail_url(); ?>" alt="">
				</div>
				<div class="col col-md-7">
					<div class="content">
						<h2>
							<?= get_field('subtitle'); ?>
						</h2>
						<?php the_content(); ?>
						<?php 
							$contacts = get_field('contacts');
							if(!empty($contacts) && count($contacts) > 0){
						?>
						<div class="small-contact">
							<h3>do you hAVE A project?</h3>
							<?php 
								foreach($contacts as $contact){
									?>
									<div class="small-row-contact">
										<span><?= $contact['contact_title']; ?> </span>
										<?php 
											if($contact['phone']){
												echo '<a href="tel:'.$contact['phone'].'">Tel: '.$contact['phone'].' </a>';
											}
											if($contact['email']){
												echo ' <a href="mailto:'.$contact['email'].'">E: '.$contact['email'].'</a> ';
											}
										?>   
									</div>
									<?php
								}
							?>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php 
				$blocks = get_field('blocks');
				if(!empty($blocks) && count($blocks) > 0){
			?>
			<div class="row panel-text-amount padding-side-xl-15">
				<?php foreach($blocks as $block){ ?>
				<div class="col col-sm-6 col-lg-4">
					<div class="panel-text">
						<h3>
							<?= $block['title']; ?>
						</h3>
						<p>
							<?= wp_trim_words($block['content'], 25, '...'); ?>
						</p>
						<button class="btn btn-sm-padding callModal" data-info='<?= getJsonBlock($block); ?>' type="button" data-toggle="modal" data-target="#modalCenter">
							FIND OUT MORE
						</button>
					</div>
				</div>
				<?php } ?>
			</div>
			<?php } ?>
		</div>
	</section>
	<?php get_template_part('partials/constructor_part/main'); ?>
	<?php 
		global $posts;
		$posts = new WP_Query(array(
			'post_type'     => 'case_studies',
			'posts_per_page'=> -1
		));
		$posts = sortStady($posts, $post->ID);
		if(count($posts) > 0){
			get_template_part('partials/works_view');
		} 
	?>
<?php get_footer();