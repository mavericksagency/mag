
</main>
<footer class="footer">
	<div class="main-footer-row">
		<div class="wrap">
			<div class="row main-footer-row__flex">
				<div class="col col-xl-3 order-top">
					<a href="/" class="footer-logo">
						<img src="<?= get_template_directory_uri() ?>/img/logo-white.svg" alt="MAG Logo">
					</a>
				</div>
				<div class="col col-md-6 col-xl-5 order-bottom">
					<?php 
						$phone = get_field('phone_number', 'option');
						if($phone){
					?>
					<a href="tel:<?= $phone ?>" class="promo">
						<span class="promo-title">Have a project?</span>
						<span class="promo-info">Call us now <?= $phone ?></span>
					</a>
					<?php } ?>
				</div>
				<div class="col col-md-6 col-xl-4 order-middle">
					<nav class="footer-nav">
						<div class="footer-nav-column">
							<h3 class="footer-menu-title">Get to know us</h3>
							<?php 
								wp_nav_menu(array(
									'theme_location'  	=> 'footer_col_1',
									'container'			=> false,
									'fallback_cb'		=> false,
									'items_wrap'      	=> '<ul class="footer-menu">%3$s</ul>',
									'depth'				=> 1
								));
							?>
						</div>
						<div class="footer-nav-column">
							<h3 class="footer-menu-title">What we do</h3>
							<?php 
								wp_nav_menu(array(
									'theme_location'  	=> 'footer_col_2',
									'container'			=> false,
									'fallback_cb'		=> false,
									'items_wrap'      	=> '<ul class="footer-menu">%3$s</ul>',
									'depth'				=> 1
								));
							?>
						</div>
					</nav>
				</div>
			</div>
		</div>	
	</div>
	<?php 
		$logos_footer = get_field('logos_in_footer', 'option');
		if(!empty($logos_footer) && count($logos_footer)){
	?>
	<div class="parnter-footer-logo">
		<div class="wrap">
			<div class="footer-slider" id="footer-slider">
				<?php 
					foreach($logos_footer as $logo_f){
						echo '<a href="'.$logo_f['url'].'" class="footer-slide"><img src="'.$logo_f['logo'].'" alt=""></a>';
					}
				?>
			</div>
		</div>
	</div>
	<?php } ?>
	<div class="copyright-row">
		<div class="wrap">
			<div class="copyright">
				<span>©2019 MAG Properties Services Ltd</span>
				<a target="_blank" href="https://www.wearebrandality.com/">Website Design & Build: Brandality</a>
			</div>
			<?php 
				$urls_f = get_field('urls', 'option');
				if(!empty($urls_f) && count($urls_f) > 0){
			?>
			<ul class="footer-privacy-menu">
				<?php foreach($urls_f as $url){ ?>
				<li>
					<a href="<?= $url['url']['url'] ?>" <?= $url['url']['target']=='_blank'?'target="_blank"':''; ?>>
						<?=  $url['url']['title'] ?>
					</a>
				</li>
				<?php } ?>
			</ul>
			<?php } ?>
		</div>
	</div>
</footer>
<?php 
get_template_part('partials/modal');
//get_template_part('partials/loader'); 
wp_footer(); ?>
</body>
</html>