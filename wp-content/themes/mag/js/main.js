$(document).ready(function() {
  /*
   * remove loader
   */
  setTimeout(function() {
    //$('.loader').addClass('fadeOut');
    $("body").removeClass("anim");
    // setTimeout(function(){
    // 	$('.loader').remove();
    // }, 700);
  }, 1000);

  mobileMenu();

  /*
   * show menu on scroll up, hover menu on scroll down
   */

  if (
    !/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(
      navigator.userAgent
    )
  ) {
    var lastScrollTop = 0;
    $(window).scroll(function(event) {
      var st = $(this).scrollTop();
      if (st > lastScrollTop) {
        $(".header").addClass("header-upper");
      } else {
        $(".header").removeClass("header-upper");
      }
      lastScrollTop = st;
    });
  } else {
    $(".header").removeClass("header-upper");
  }

  $("#footer-slider").slick({
    swipeToSlide: true,
    infinite: false,
    dots: false,
    arrows: false,
    // variableWidth: true,
    slidesToShow: 6,
    slidesToScroll: 6,
    autoplay: true,
    autoplaySpeed: 2500,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }
    ]
  });

  $("#carousel").slick({
    infinite: false,
    dots: true,
    arrows: false,
    // variableWidth: true,
    slidesToShow: 6,
    slidesToScroll: 6,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [
      {
        breakpoint: 1080,
        settings: {
          slidesToShow: 1,
          swipeToSlide: true
        }
      }
    ]
  });

  $("#main-slider").slick({
    infinite: false,
    dots: false,
    arrows: false,
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 5000
  });

  /*
   * add animation when scroll
   */
  setTimeout(function() {
    removeAnim();
    $(window).on("scroll", function() {
      removeAnim();
    });
  }, 800);

  $(".callModal").on("click", function() {
    var data = JSON.parse($(this).attr("data-info"));
    $("#modalCenter .content").html(data["content"]);
    $("#modalCenter .modal-title").text(data["title"]);
    $("#modalCenter .phone").attr("href", "tel:" + data["phone"]);
    $("#modalCenter .phone .number").text(data["phone"]);
    $("#modalCenter").modal("show");
  });

  $(".modal-body").on("click", ".img-block", function(event) {
    if (!$(this).hasClass("image-extended")) {
      var imageExtend = $(this).clone();
      imageExtend.addClass("image-extended");
      imageExtend.append('<button type="button" class="close-modal"></button>');
      $("body").append(imageExtend);
    } else {
    }
  });

  $("body").on("click", ".img-block.image-extended", function(event) {
    $(".image-extended").remove();
  });
});

/*
 * remove class anim-block, when user see the element
 */
function removeAnim() {
  var windowTop = $(window).scrollTop();
  var windowHeight = $(window).height();
  $(".anim-block").each(function() {
    if (windowTop + windowHeight * 0.7 > $(this).offset().top) {
      $(this).removeClass("anim-block");
    }
  });
}

/*
 * allows open/close menu and submenu in mobile
 */
function mobileMenu() {
  $("#btn-menu").on("click", function() {
    $("#menu-row").toggleClass("visible");
    $("#btn-menu").toggleClass("open-menu");
    $(".menu-item-has-children>a, .menu-item-has-children>span").removeClass(
      "open-submenu"
    );
    $("body").toggleClass("body-overflow");
  });

  if (
    /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(
      navigator.userAgent
    )
  ) {
    $(".menu-item-has-children>a, .menu-item-has-children>span").on(
      "click",
      function(e) {
        if (!$(this).hasClass("open-submenu")) {
          e.preventDefault();
          $(
            ".menu-item-has-children>a, .menu-item-has-children>span"
          ).removeClass("open-submenu");
          $(this).addClass("open-submenu");
        }
        $(document).on("click", function(event) {
          if (
            !$(event.target).closest(".menu-item-has-children .open-submenu")
              .length
          ) {
            $(".menu-item-has-children .open-submenu").removeClass(
              "open-submenu"
            );
          }
        });
      }
    );
  }
}
