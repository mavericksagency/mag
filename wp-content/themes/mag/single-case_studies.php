<?php 
	get_header();
	the_post();
	get_template_part('partials/title');
?>
<section class="section section-content section-negative-margin anim-block transformY-top">
	<div class="wrap">
		<div class="row">
			<div class="col col-md-4 col-xl-6">
				<?php 
					$gallery = get_field('gallery');
					if(!empty($gallery) && count($gallery) > 0){
				?>
				<div class="image-gallery">
					<?php 
						foreach($gallery as $img){
							echo '<img src="'.$img['url'].'" alt="">';
						}
					?>
				</div>
				<?php } ?>
			</div>
			<div class="col col-md-8 col-xl-5">
				<div class="case-content">
					<div class="info-table">
						<?php 
							$location = get_field('location');
							if($location){
						?>
						<div class="info-row">
							<span class="info-title">Location:</span>
							<span class="info-value"><?= $location; ?></span>
						</div>
						<?php } ?>
						<?php 
							$client = get_field('client');
							if($client){
						?>
						<div class="info-row">
							<span class="info-title">Client:</span>
							<span class="info-value"><?= $client; ?></span>
						</div>
						<?php } ?>
						<?php 
							$work_undertaken = get_field('work_undertaken');
							if(!empty($work_undertaken) && count($work_undertaken) > 0){
						?>
						<div class="info-row">
							<span class="info-title">WORK UNDERTAKEN:</span>
							<span class="info-value">
								<?php 
									foreach($work_undertaken as $item){
										echo '<span>'.$item['item'].'</span>';
									}
								?>
							</span>
						</div>
						<?php } ?>
					</div>
					<div class="content">
						<?php the_content(); ?>
					</div>
					<?php 
						$phone = get_field('phone_number');
						if($phone){
					?>
					<a href="tel:<?= $phone ?>" class="tel-link">
						<span class="tel-link-title">HAVE A SIMILAR project?</span>
						<span class="tel-link-contact">
							contact us <span><?= $phone ?></span>
						</span>
					</a>
					<?php } ?>
					<a href="<?= get_permalink(173); ?>" class="btn">
						View more case
					</a>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer();