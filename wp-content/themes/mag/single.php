<?php 
get_header();
the_post();
get_template_part('partials/title');
?>
	<section class="section section-article section-negative-margin">
		<div class="wrap">
			<div class="row">
				<div class="col col-lg-9 col-xl-8">
					
					<div class="article-title padding-side-xl-15">
						<h1 class="h1 m-b-3"><?= $post->post_title; ?></h1>
						<?php 
							// if($subtitle = get_field('subtitle')){
							// 	echo '<h2>'.$subtitle.'</h2>';
							// }
						?>
						<span class="article-date"><?= get_the_date('j F Y', $post); ?></span>
					</div>
					<div class="content clearfix padding-side-xl-15">
						<?php 
							if(has_post_thumbnail($post->ID)){
								echo '<img src="'.get_the_post_thumbnail_url().'" alt=""/>';
							}
						?>
						<?php the_content(); ?>
					</div>
					<div class="arrow-pagination d-flex justify-content-between">
						
						<?php  
							//prev
							$prev_post = get_previous_post();
							if($prev_post->ID){
								echo '<a href="'.get_permalink($prev_post->ID).'" class="arrow-link">
											« Previous
										</a>';
							}
							//next
							$next_post = get_next_post();
							if($next_post->ID){
								echo '<a href="'.get_permalink($next_post->ID).'" class="arrow-link">
											Next »
										</a>';
							}
						?>
					</div>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</section>
<?php get_footer();
