<?php 
/**
 * Template Name: Home
 */
get_header(); ?>

	<section class="section main-section">
        <?php 
            get_template_part('partials/constructor_part/main');
            global $posts;
            $posts = new WP_Query(array(
                'post_type'     => 'case_studies',
                'posts_per_page'=> -1
            ));
            $posts = $posts->posts;
            if($posts){
                get_template_part('partials/works_view');
            } 
        ?>
	</section>

<?php 
get_footer();