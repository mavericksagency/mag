<?php 
/**
 * Template Name: News
 */
get_header(); 
the_post();
get_template_part('partials/title');
?>
<section class="section section-content section-negative-margin anim-block transformY-top">
	<div class="wrap">
		<div class="row">
			<div class="col col-lg-9 col-xl-8">
				<div class="news-amount">
					<?php 
						global $paged, $query;
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						$query = new WP_Query(array(
							'post_type'			=> 'post',
							'posts_per_page'	=> 10,
							'order'				=> 'DESC',
							'paged'				=> $paged
						));

						if($query->posts){
							while($query->have_posts()){
								$query->the_post();
								get_template_part('partials/news/post');
							}
							wp_reset_postdata();
						}
					?>
				</div>
				<?php 
					if($query->posts){
						pagination();
					}
				?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php 
get_template_part('partials/constructor_part/main'); 
get_footer();
