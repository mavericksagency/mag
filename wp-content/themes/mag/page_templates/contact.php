<?php 
/**
 * Template Name: Contacts
 */
get_header();
the_post();
get_template_part('partials/title');
?>
	<section class="section section-content section-negative-margin anim-block transformY-top">
		<div class="wrap">
			<div class="row">
				<div class="col col-sm-6 col-lg-5">
					<img src="<?= get_the_post_thumbnail_url(); ?>" alt="">
				</div>
				<div class="col col-sm-6 col-lg-7">
					<div class="adress">
						<?= the_content(); ?>
					</div>
					<?php 
						$main_phone = get_field('main_office_phone');
						if($main_phone){
							echo '<a href="tel:'.$main_phone.'" class="contacts-phone">Main Office: '.$main_phone.'</a>';
						}
					?>
				</div>
			</div>
			<div class="adress-grid">
				<div class="row">
					<?php 
						$locations = get_field('locations');
						if(!empty($locations) && count($locations) > 0){
							foreach($locations as $loc){
								?>
								<div class="col col-sm-6 col-lg-3">
									<div class="adress-col">
										<h3>
											<?= $loc['location']; ?>
										</h3>
										<?php 
											if(!empty($loc['contacts']) && count($loc['contacts']) > 0){
												foreach($loc['contacts'] as $contact){
													?>
													<div class="person-adress">
														<h4>
															<?= $contact['contact_person'] ?>    
														</h4>
														<a href="tel:<?= $contact['phone'] ?> ">
															Tel: <?= $contact['phone'] ?>
														</a>
														<a href="mailto:<?= $contact['email'] ?>">Email: <?= $contact['email'] ?> </a>
													</div>
													<?php
												}
											}
										?>
									</div>
								</div>
								<?php
							}
						}
					?>
				</div>
			</div>
		</div>
	</section>

	<section class="section section-contact">
		<div class="wrap-sm">
			<h2>
				Fill in the form and we will contact you
			</h2>
			<?php the_field('form') ?>
		</div>
	</section>

<?php get_footer();