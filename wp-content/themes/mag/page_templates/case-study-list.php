<?php 
/**
 * Template Name: Case study list
 */
	get_header();
	the_post();
	get_template_part('partials/title');
?>
	<section class="section section-content section-negative-margin anim-block transformY-top">
		<div class="wrap">
			<div class="wrap-sm padding-md-top-40">
				<?php the_content(); ?>
			</div>
			<?php 
				global $paged, $query;
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$query = new WP_Query(array(
					'post_type'			=> 'case_studies',
					'posts_per_page'	=> 12,
					'order'				=> 'DESC',
					'paged'				=> $paged
				));
			?>
			<div class="case-list">
				<div class="row">
					<?php 
						if($query->posts){
							while($query->have_posts()){
								$query->the_post();
								get_template_part('partials/case_study/post');
							}
							wp_reset_postdata();
						}
					?> 
				</div>
			</div> 
			<?php 
				if($query->posts){
					pagination();
				}
			?>
		</div>
	</section>
	<?php get_template_part('partials/constructor_part/main'); ?>
<?php get_footer();