<?php 
/**
 * Template Name: About
 */
get_header();
the_post();
get_template_part('partials/title');
?>
	<section class="section section-content section-negative-margin anim-block transformY-top">
		<div class="wrap">
			<div class="row">
				<div class="col col-lg-5">
					<img class="about-image text-center d-block" src="<?= get_the_post_thumbnail_url(); ?>" alt="">
				</div>
				<div class="col col-lg-7">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part('partials/constructor_part/main'); ?>
<?php get_footer();