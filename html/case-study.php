<?php include('header.php'); ?>
<?php $title = 'Hacienda apartments'; include('inc/title.php'); ?>

	<section class="section section-content section-negative-margin anim-block transformY-top">
		<div class="wrap">
			<div class="row">
				<div class="col col-md-4 col-xl-6">
					<div class="image-gallery">
						<img src="img/content-img/image-gallery1.jpg" alt="">
						<img src="img/content-img/image-gallery2.jpg" alt="">
						<img src="img/content-img/image-gallery3.jpg" alt="">
					</div>
				</div>
				<div class="col col-md-8 col-xl-5">
					<div class="case-content">
						<div class="info-table">
							<div class="info-row">
								<span class="info-title">Location:</span>
								<span class="info-value">Manchester, UK</span>
							</div>
							<div class="info-row">
								<span class="info-title">Client:</span>
								<span class="info-value">Balfour Beatty</span>
							</div>
							<div class="info-row">
								<span class="info-title">WORK UNDERTAKEN:</span>
								<span class="info-value">
									<span>LED emergency lighting</span>
									<span>Refrigeration Removal</span>
									<span>Water isolations</span>
									<span>Gas isolations</span>
								</span>
							</div>
						</div>
						<div class="content">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
							</p>
							<p>
								Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
							</p>
						</div>
						<a href="tel:01279452737" class="tel-link">
							<span class="tel-link-title">HAVE A SIMILAR project?</span>
							<span class="tel-link-contact">
								contact us <span>01279 452737</span>
							</span>
						</a>
						<a href="#" class="btn">
							View more case
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>


<?php include('footer.php'); ?>