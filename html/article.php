<?php include('header.php'); ?>

<?php $title = 'News'; include('inc/title.php'); ?>

	<section class="section section-article section-negative-margin">
		<div class="wrap">
			<div class="row">
				<div class="col col-lg-9 col-xl-8">
					<div class="article-title padding-side-xl-15">
						<h2>Lorem ipsum dolor sit ut enim ad minim</h2>
						<span class="article-date">29 February 2019</span>
					</div>
					<div class="content clearfix padding-side-xl-15">
						<img src="img/content-img/news-article-img.jpg" alt="">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa beatae eos magnam iure, consectetur! Dolorum laborum vero delectus a ea! Sapiente doloremque numquam eligendi blanditiis maiores, molestiae repudiandae consequatur. Provident nisi impedit numquam animi repellendus saepe quibusdam quas debitis facilis, quod dolor soluta, quaerat consequuntur qui sunt doloremque consequatur deserunt fugit nostrum temporibus repellat tempore, exercitationem voluptatibus. Id porro, aut recusandae provident officia! Molestias, maxime. Iste, ab vero. Eveniet optio ea perferendis laboriosam ipsa, voluptatem qui, voluptates, odit laudantium, rerum tempora corporis ab placeat illo consectetur aliquid cumque nostrum magnam modi quae. Est, placeat. Dolores fugiat illo nam, architecto culpa?
						</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda dicta quae iusto quam itaque, debitis cumque ex ducimus vel accusamus ea sapiente sint ipsa! Animi incidunt earum quaerat atque debitis.
						</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum doloremque sequi animi quo, vitae facere blanditiis nemo consectetur maxime, cumque. Eum saepe totam blanditiis, aut hic dolorem ipsa repudiandae ab quasi amet neque, quaerat. Corporis facilis quis nulla iure, assumenda doloribus, repellendus quae aspernatur impedit facere accusantium maiores aperiam tenetur.
						</p>
					</div>
					<div class="arrow-pagination d-flex justify-content-between">
						<a href="#" class="arrow-link">
							« Previous
						</a>
						<a href="#" class="arrow-link">
							Next »
						</a>
					</div>
				</div>
				<div class="col col-lg-3 col-xl-4">
					<div class="aside">
						<div class="aside-block">
							<h3 class="aside-title">
								Categories
							</h3>
							<ul class="aside-list text-uppercase">
								<li><a href="#">News</a></li>
							</ul>
						</div>

						<div class="aside-block">
							<h3 class="aside-title">
								Archive
							</h3>
							<ul class="aside-list">
								<li><a href="#">February 2019</a></li>
								<li><a href="#">January 2019</a></li>
								<li><a href="#">December 2018</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php include('footer.php'); ?>
