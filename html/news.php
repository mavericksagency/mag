<?php include('header.php'); ?>
<?php $title = 'News'; include('inc/title.php'); ?>

	<section class="section section-content section-negative-margin anim-block transformY-top">
		<div class="wrap">
			<div class="row">
				<div class="col col-lg-9 col-xl-8">
					<div class="news-amount">
						<div class="news-item">
							<div class="news-item-inner">
								<h2>
									<a href="#">
										Lorem ipsum dolor sit amet consectetur 
									</a>
								</h2>
								<span class="news-date">
									29 February 2019
								</span>
								<div class="news-description">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam ipsam minima libero quibusdam, voluptatum eum eius odit, cum quia maxime repellendus earum illo, ut laudantium ad dolorum fugit unde sunt dolores aliquid necessitatibus velit culpa cumque debitis. Assumenda doloremque voluptatum error ipsum, ratione ullam, minima optio maiores quisquam corrupti libero.
								</div>
								<a href="#" class="btn-more">
									Read more
								</a>
							</div>
						</div>
						<div class="news-item">
							<img src="img/content-img/news-article-img.jpg" alt="">
							<div class="news-item-inner">
								<h2>
									<a href="#">
										Lorem ipsum dolor sit amet consectetur 
									</a>
								</h2>
								<span class="news-date">
									29 February 2019
								</span>
								<div class="news-description">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam ipsam minima libero quibusdam, voluptatum eum eius odit, cum quia maxime repellendus earum illo, ut laudantium ad dolorum fugit unde sunt dolores aliquid necessitatibus velit culpa cumque debitis. Assumenda doloremque voluptatum error ipsum, ratione ullam, minima optio maiores quisquam corrupti libero.
								</div>
								<a href="#" class="btn-more">
									Read more
								</a>
							</div>
						</div>
						<div class="news-item">
							<div class="news-item-inner">
								<h2>
									<a href="#">
										Lorem ipsum dolor sit amet consectetur 
									</a>
								</h2>
								<span class="news-date">
									29 February 2019
								</span>
								<div class="news-description">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam ipsam minima libero quibusdam, voluptatum eum eius odit, cum quia maxime repellendus earum illo, ut laudantium ad dolorum fugit unde sunt dolores aliquid necessitatibus velit culpa cumque debitis. Assumenda doloremque voluptatum error ipsum, ratione ullam, minima optio maiores quisquam corrupti libero.
								</div>
								<a href="#" class="btn-more">
									Read more
								</a>
							</div>
						</div>
					</div>
					<div class="pagination">
						<!-- <a href="#" class="prev pagination-arrow">«</a> -->
						<a href="#" class="current">1</a>
						<a href="#">2</a>
						<a href="#" class="next pagination-arrow">»</a>
					</div>
				</div>
				<div class="col col-lg-3 col-xl-4">
					<div class="aside">
						<div class="aside-block">
							<h3 class="aside-title">
								Categories
							</h3>
							<ul class="aside-list text-uppercase">
								<li><a href="#">News</a></li>
							</ul>
						</div>

						<div class="aside-block">
							<h3 class="aside-title">
								Archive
							</h3>
							<ul class="aside-list">
								<li><a href="#">February 2019</a></li>
								<li><a href="#">January 2019</a></li>
								<li><a href="#">December 2018</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php include('footer.php'); ?>
