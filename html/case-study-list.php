<?php include('header.php'); ?>
<?php $title = 'Mechanical'; include('inc/title.php'); ?>
	<section class="section section-content section-negative-margin anim-block transformY-top">
		<div class="wrap">
			<div class="wrap-sm padding-md-top-40">
				<h2>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
				</h2>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
				</p>
			</div>
			<div class="case-list">
				<div class="row">
					<div class="col col-sm-6 col-md-4">
						<div class="case-panel">
							<a href="#" class="case-panel-img">
								<img src="img/content-img/image-gallery1.jpg" alt="" class="cover-img">
							</a>
							<div class="case-panel-title">
								<a href="#" class="case-title">Hacienda Apartments</a>
								<span class="case-location">
									Manchester, UK
								</span>
							</div>
							<a href="#" class="btn case-link">
								View casestudy
							</a>
						</div>
					</div>
					<div class="col col-sm-6 col-md-4">
						<div class="case-panel">
							<a href="#" class="case-panel-img">
								<img src="img/content-img/image-gallery1.jpg" alt="" class="cover-img">
							</a>
							<div class="case-panel-title">
								<a href="#" class="case-title">Hacienda Apartments</a>
								<span class="case-location">
									Manchester, UK
								</span>
							</div>
							<a href="#" class="btn case-link">
								View casestudy
							</a>
						</div>
					</div>
					<div class="col col-sm-6 col-md-4">
						<div class="case-panel">
							<a href="#" class="case-panel-img">
								<img src="img/content-img/image-gallery1.jpg" alt="" class="cover-img">
							</a>
							<div class="case-panel-title">
								<a href="#" class="case-title">Hacienda Apartments</a>
								<span class="case-location">
									Manchester, UK
								</span>
							</div>
							<a href="#" class="btn case-link">
								View casestudy
							</a>
						</div>
					</div>
					<div class="col col-sm-6 col-md-4">
						<div class="case-panel">
							<a href="#" class="case-panel-img">
								<img src="img/content-img/image-gallery1.jpg" alt="" class="cover-img">
							</a>
							<div class="case-panel-title">
								<a href="#" class="case-title">Hacienda Apartments</a>
								<span class="case-location">
									Manchester, UK
								</span>
							</div>
							<a href="#" class="btn case-link">
								View casestudy
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('inc/services.php'); ?>


<?php include('footer.php');?>