<style>
	.btn-dev{
		min-width: auto;
		padding: 1rem;
		font-size: 1.2rem;
		line-height: 1.2rem;
		position: fixed;
		z-index: 500;
		bottom: 1rem;
		right: 1rem;
	}
	.dev-menu{
		position: fixed;
		bottom: 6rem;
		right: 1rem;
		background-color: #65758c;
		list-style-type: none;
		margin: 0;
		padding: 0;
		width: 200px;
		z-index: 9999999;
		max-height: 80vh;
		overflow-y: auto;
	}

	.dev-menu li a{
		padding: 10px 20px;
		text-decoration: none;
		font-size: 16px;
		font-weight: 600;
		color: #fff;
		display: block;
	}
</style>

<button class="btn btn-dev" type="button" id="btn-dev">
	dev-menu
</button>

<ul class="dev-menu d-none" id="dev-menu">
	<li>
		<a href="/404.php">
			404
		</a>
	</li>
	<li>
		<a href="/about.php">
			About
		</a>
	</li>
	<li>
		<a href="/article.php">
			Article
		</a>
	</li>
	<li>
		<a href="/case-study-list.php">
			Case study list
		</a>
	</li>
	<li>
		<a href="/case-study.php">
			Case study
		</a>
	</li>
	<li>
		<a href="/contact.php">
			Contact
		</a>
	</li>
	<li>
		<a href="/news.php">
			News
		</a>
	</li>
	<li>
		<a href="/sector-page.php">
			Sector Page
		</a>
	</li>
	<li>
		<a href="/simple-page.php">
			Simple Page
		</a>
	</li>
	<li>
		<a href="/simple-page-type2.php">
			Simple Page Type 2
		</a>
	</li>
	<li>
		<a href="/index.php">
			Home Page
		</a>
	</li>
</ul>

<script>
	var btnDev = document.getElementById('btn-dev');
	var devMenu = document.getElementById('dev-menu');
	document.addEventListener('keypress', function(e){
		if(e.keyCode == 113){
			devMenu.classList.toggle('d-none');
		}
	});

	btnDev.addEventListener('click', function(){
		devMenu.classList.toggle('d-none');
	});

</script>