<section class="section anim-block transformY-top" >
	<div class="row margin-side-none align-items-stretch">
		<div class="col col-lg-5">
			<div class="service-left">
				<img src="../img/content-img/services.jpg" alt="">
				<div class="service-inner service-left-inner">
					<h3>Health & safety at the heart of what we do</h3>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
					</p>
					<a href="#" class="btn service-link">Find out more</a>
				</div>
			</div>
		</div>
		<div class="col col-lg-7">
			<div class="service-right">
				<div class="service-inner service-right-inner">
					<h2>OUR SERVICES</h2>
					<div class="service-list">
						<div class="row margin-side-none">
							<div class="col col-md-6">
								<div class="service-item">
									<h3>
										MECHANICAL
									</h3>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
									</p>
									<a href="#" class="btn service-link dark">Find out more</a>
								</div>
							</div>
							<div class="col col-md-6">
								<div class="service-item">
									<h3>
										Electrical
									</h3>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
									</p>
									<a href="#" class="btn service-link dark">Find out more</a>
								</div>
							</div>
							<div class="col col-md-6">
								<div class="service-item">
									<h3>
										environmental
									</h3>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
									</p>
									<a href="#" class="btn service-link dark">Find out more</a>
								</div>
							</div>
							<div class="col col-md-6">
								<div class="service-item">
									<h3>
										HIRE PLANT
									</h3>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
									</p>
									<a href="#" class="btn service-link dark">Find out more</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>