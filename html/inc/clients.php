<section class="section section-carousel anim-block transformY-top">
	<div class="wrap">
		<h2>
			SOME OF OUR CLIENTS
		</h2>
		<div class="section-desc">
			We work with some of the best known and highly regarded names in the industry. We’re proud to build good working relationships that endure.
		</div>
		<div class="carousel" id="carousel">
			<div class="carousel-item">
				<img src="../img/clients/01.jpg" alt="">
			</div>
			<div class="carousel-item">
				<img src="../img/clients/02.jpg" alt="">
			</div>
			<div class="carousel-item">
				<img src="../img/clients/03.jpg" alt="">
			</div>
			<div class="carousel-item">
				<img src="../img/clients/04.jpg" alt="">
			</div>
			<div class="carousel-item">
				<img src="../img/clients/05.jpg" alt="">
			</div>
			<div class="carousel-item">
				<img src="../img/clients/06.jpg" alt="">
			</div>
			<div class="carousel-item">
				<img src="../img/clients/01.jpg" alt="">
			</div>
			<div class="carousel-item">
				<img src="../img/clients/02.jpg" alt="">
			</div>
			<div class="carousel-item">
				<img src="../img/clients/03.jpg" alt="">
			</div>
			<div class="carousel-item">
				<img src="../img/clients/04.jpg" alt="">
			</div>
			<div class="carousel-item">
				<img src="../img/clients/05.jpg" alt="">
			</div>
			<div class="carousel-item">
				<img src="../img/clients/06.jpg" alt="">
			</div>
		</div>
	</div>
</section>