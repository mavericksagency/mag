<section class="section section-work anim-block transformY-top">
	<div class="wrap">
		<h2>
			View our work
		</h2>
		<div class="row">
			<div class="col col-sm-6 col-md-4">
				<div class="case-panel">
					<a href="#" class="case-panel-img">
						<img src="img/content-img/image-gallery1.jpg" alt="" class="cover-img">
					</a>
					<div class="case-panel-title">
						<a href="#" class="case-title">Hacienda Apartments</a>
						<span class="case-location">
							Manchester, UK
						</span>
					</div>
					<a href="#" class="btn case-link">
						View casestudy
					</a>
				</div>
			</div>
			<div class="col col-sm-6 col-md-4">
				<div class="case-panel">
					<a href="#" class="case-panel-img">
						<img src="img/content-img/image-gallery1.jpg" alt="" class="cover-img">
					</a>
					<div class="case-panel-title">
						<a href="#" class="case-title">Hacienda Apartments</a>
						<span class="case-location">
							Manchester, UK
						</span>
					</div>
					<a href="#" class="btn case-link">
						View casestudy
					</a>
				</div>
			</div>
			<div class="col col-sm-6 col-md-4">
				<div class="case-panel">
					<a href="#" class="case-panel-img">
						<img src="img/content-img/image-gallery1.jpg" alt="" class="cover-img">
					</a>
					<div class="case-panel-title">
						<a href="#" class="case-title">Hacienda Apartments</a>
						<span class="case-location">
							Manchester, UK
						</span>
					</div>
					<a href="#" class="btn case-link">
						View casestudy
					</a>
				</div>
			</div>
		</div>
	</div>
</section>