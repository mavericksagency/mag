<?php include('header.php'); ?>

	<section class="section main-section">
		<div class="main-slider anim-block transformY-top" id="main-slider">
			<div class="main-slider-item">
				<img src="img/content-img/slider.jpg" alt="" class="cover-img">
				<div class="slider-content">
					<span class="upper-slider-text">
						CASE STUDIES
					</span>
					<h2 class="main-slider-title">
						Site Services
					</h2>
					<span class="main-slider-desc">
						we dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor 

					</span>
				</div>
			</div>
			<div class="main-slider-item">
				<img src="img/content-img/slider.jpg" alt="" class="cover-img">
				<div class="slider-content">
					<span class="upper-slider-text">
						CASE STUDIES
					</span>
					<h2 class="main-slider-title">
						Site Services
					</h2>
					<span class="main-slider-desc">
						we dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
					</span>
				</div>
			</div>
			<div class="main-slider-item">
				<img src="img/content-img/slider.jpg" alt="" class="cover-img">
				<div class="slider-content">
					<span class="upper-slider-text">
						CASE STUDIES
					</span>
					<h2 class="main-slider-title">
						Site Services
					</h2>
					<span class="main-slider-desc">
						we dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
					</span>
				</div>
			</div>
		</div>

		<div class="main-section-info anim-block transformY-top">
			<div class="wrap">
				<div class="row">
					<div class="col col-md-5">
						<h1 class="main-title">
							MAG proudly deliver a wide variety of projects safely, on schedule and on budget
						</h1>
					</div>
					<div class="col col-md-7">
						<h3>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud elit, sed do eiusmod tempor incididunt ut labore et dolore.
						</h3>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam 
						</p>
						<a href="#" class="btn  btn-sm-padding">
							Find out more
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('inc/services.php'); ?>
	<?php include('inc/clients.php'); ?>
	<?php include('inc/works-view.php'); ?>

	


<?php include('footer.php'); ?>