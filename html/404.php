<?php include('header.php'); ?>
	<section class="section section-negative-margin section-error">
		<div class="wrap">
			<h1>
				404
			</h1>
			<span class="text">Sorry, the page you are looking for wasn't found.</span>
			<a href="#" class="btn">
				Back to home page
			</a>
		</div>
	</section>
<?php include('footer.php'); ?>