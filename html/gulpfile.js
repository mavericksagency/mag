const { src, dest, watch, series, parallel } = require('gulp');
const browsersync = require('browser-sync').create();
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');

function buildStyles() {
  return src('scss/*')
    .pipe(sass())
    .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7']))
    .pipe(dest('css/'))
    .pipe(browsersync.reload({ stream: true }));
}


function watchFiles() {
  watch(
    ['scss/*', 'scss/*/*', '*/*.php', '*.php'],
    { events: 'all', ignoreInitial: false },
    series(buildStyles),
  );
}


function browserSync(done) {
  browsersync.init({
    proxy: 'mag.loc',
    socket: {
      domain: 'localhost:3000'
    }
  });
  watch('*.php').on('change', browsersync.reload);
  watch('js/*').on('change', browsersync.reload);
  done();
}


exports.default = parallel(browserSync, watchFiles);
exports.sass = buildStyles;
exports.watch = watchFiles;
exports.build = series(buildStyles);