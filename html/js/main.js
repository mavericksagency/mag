$(document).ready(function(){

	/*
	* remove loader
	*/
	setTimeout(function(){
		$('.loader').addClass('fadeOut');
		$('body').removeClass('anim');
		setTimeout(function(){
			$('.loader').remove();
		}, 700);
	}, 1000);

	mobileMenu();

	/*
	* show menu on scroll up, hover menu on scroll down 
	*/
	var lastScrollTop = 0;
	$(window).scroll(function(event){
		var st = $(this).scrollTop();
		if (st > lastScrollTop){
			$('.header').addClass('header-upper');
		} else {
			$('.header').removeClass('header-upper');
		}
		lastScrollTop = st;
	});

	$('#footer-slider').slick({
		swipeToSlide: true,
		infinite: false,
		dots: false,
		arrows: false,
		variableWidth: true,
	});

	$('#carousel').slick({
		infinite: false,
		dots: true,
		arrows: false,
		variableWidth: true,
		slidesToShow: 6,
		slidesToScroll: 6,
		responsive: [
			{
				breakpoint: 1080,
				settings: {
					slidesToShow: 1,
					swipeToSlide: true,
				}
			},
		]
	});

	$('#main-slider').slick({
		infinite: false,
		dots: false,
		arrows: false,
		slidesToShow: 1,
	});

	/*
	* add animation when scroll
	*/
	setTimeout(function(){
		removeAnim();
		$(window).on('scroll', function(){
			removeAnim();
		});
	}, 800);



	$('.img-block').on('click', function(event){
		if(!$(this).hasClass('image-extended')){
			var imageExtend = $(this).clone();
			imageExtend.addClass('image-extended');
			$('body').append(imageExtend);
		} else {

		}
	});

	$(document).on('click', function(event){
		if ($(event.target).closest(".image-extended").length){
			$(".image-extended").remove();
		}
	});
		
	
});


/*
* remove class anim-block, when user see the element
 */
function removeAnim(){
	var windowTop = $(window).scrollTop();
	var windowHeight = $(window).height();
	$('.anim-block').each(function(){
		if(windowTop + windowHeight*0.7 > $(this).offset().top){
			$(this).removeClass('anim-block');

		}
	});
}

/*
* allows open/close menu and submenu in mobile
*/
function mobileMenu(){
	$('#btn-menu').on('click', function(){
		$("#menu-row").toggleClass('visible');
		$('#btn-menu').toggleClass('open-menu');
		$('.has-submenu>a, .has-submenu>span').removeClass('open-submenu');
		$('body').toggleClass('body-overflow');
	});

	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
		
		$('.has-submenu>a, .has-submenu>span').on('click', function(e){
			if(!$(this).hasClass('open-submenu')){
				e.preventDefault();
				$('.has-submenu>a, .has-submenu>span').removeClass('open-submenu');
				$(this).addClass('open-submenu');
			} 
			$(document).on('click', function(event){
				if (!$(event.target).closest(".has-submenu .open-submenu").length){
					$('.has-submenu .open-submenu').removeClass('open-submenu');
				}
			});

		});
	}
}