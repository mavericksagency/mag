<?php include('header.php'); ?>
<?php $title = 'PRIVACY & COOKIES'; include('inc/title.php'); ?>

	<section class="section section-content section-negative-margin">
		<div class="wrap">
			<div class="row padding-side-xl-15">
				<div class="col col-md-5 text-center">
					<img src="img/content-img/simple-page-img.jpg" alt="">
				</div>
				<div class="col col-md-7">
					<div class="content">
						<h2>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel, ducimus.
						</h2>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto veritatis, voluptas harum? Ad libero, dolorum quae ratione quam suscipit non reiciendis doloribus accusantium dignissimos voluptate enim ipsum nulla autem officia!
						</p>
					</div>
				</div>
			</div>
			<div class="content">
				<div class="wrap-sm">
					<h2>Lorem ipsum dolor</h2>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum aperiam obcaecati ipsam esse laudantium numquam explicabo deleniti earum quia quam fugiat iusto quasi placeat quae, optio ut recusandae sed, consequuntur quaerat beatae accusantium eaque amet natus commodi. Amet, sequi incidunt.
					</p>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est, maxime?
					</p>
					<h3>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit.
					</h3>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet ipsum, error quia esse inventore asperiores officia animi quis perferendis quas libero dicta consequatur, at officiis similique molestiae illum quidem?
					</p>
					<h2>Lorem ipsum dolor</h2>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum aperiam obcaecati ipsam esse laudantium numquam explicabo deleniti earum quia quam fugiat iusto quasi placeat quae, optio ut recusandae sed, consequuntur quaerat beatae accusantium eaque amet natus commodi. Amet, sequi incidunt.
					</p>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est, maxime?
					</p>
					<h3>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit.
					</h3>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet ipsum, error quia esse inventore asperiores officia animi quis perferendis quas libero dicta consequatur, at officiis similique molestiae illum quidem?
					</p>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet ipsum, error quia esse inventore asperiores officia animi quis perferendis quas libero dicta consequatur, at officiis similique molestiae illum quidem?
					</p>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet ipsum, error quia esse inventore asperiores officia animi quis perferendis quas libero dicta consequatur, at officiis similique molestiae illum quidem?
					</p>
					<h2>Lorem ipsum dolor</h2>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum aperiam obcaecati ipsam esse laudantium numquam explicabo deleniti earum quia quam fugiat iusto quasi placeat quae, optio ut recusandae sed, consequuntur quaerat beatae accusantium eaque amet natus commodi. Amet, sequi incidunt.
					</p>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet ipsum, error quia esse inventore asperiores officia animi quis perferendis quas libero dicta consequatur, at officiis similique molestiae illum quidem?
					</p>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est, maxime?
					</p>
					<h3>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit.
					</h3>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet ipsum, error quia esse inventore asperiores officia animi quis perferendis quas libero dicta consequatur, at officiis similique molestiae illum quidem?
					</p>
				</div>
			</div>
		</div>
	</section>
	

<?php include('footer.php'); ?>