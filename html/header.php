<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title>MAG</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css">
	<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
	<link rel="stylesheet" href="bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="slick/slick.css">
	<link rel="stylesheet" href="css/styles.css">
</head>
<body class="anim">

	<div class="d-none">
		<?php include('inc/svgmap.php'); ?>
	</div>

	<header class="header"> 
		<div class="wrap">
			<a href="#" class="logo">
				<img src="img/logo.svg" alt="Mag Logo">
			</a>
			<button class="btn-tr btn-menu" id="btn-menu" type="button">
				<svg class="icon menu-icon">
					<use xlink:href="#menu">
				</svg>
				<svg class="icon close-icon">
					<use xlink:href="#close">
				</svg>
			</button>
			<div class="header-row" id="menu-row">
				<nav class="nav">
					<ul class="main-menu">
						<li><a href="#">Home</a></li>
						<li><a href="#">About</a></li>
						<li class="has-submenu"><a href="#">Services</a>
							<ul class="submenu">
								<li><a href="#">Mechanical</a></li>
								<li><a href="#">Electrical</a></li>
								<li><a href="#">Environmental</a></li>
								<li><a href="#">Hire plant</a></li>
								<li><a href="#">Enabling works</a></li>
							</ul>
						</li>
						<li><a href="#">Case Studies</a></li>
						<li><a href="#">News</a></li>
						<li><a href="#">Contact</a></li>
					</ul>
				</nav>
				<a href="tel:01279452737" class="call">
					Call 01279 452737
				</a>
			</div>
		</div>
	</header>

	<main class="main" id="main">