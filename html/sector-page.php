<?php include('header.php'); ?>
<?php $title = 'MECHANICAL'; include('inc/title.php'); ?>

	<section class="section section-content section-md-margin-bottom-none section-negative-margin anim-block transformY-top">
		<div class="wrap">
			<div class="row padding-side-xl-15">
				<div class="col col-md-5 text-center">
					<img src="img/content-img/simple-page-img.jpg" alt="">
				</div>
				<div class="col col-md-7">
					<div class="content">
						<h2>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
						</h2>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
						</p>
						<div class="small-contact">
							<h3>do you hAVE A project?</h3>
							<div class="small-row-contact">
								<span>James Gull</span>
								<a href="tel:07831 331269">Tel: 07831 331269 </a>
								<a href="mailto:james@magltd.co.uk">E: james@magltd.co.uk</a>       
							</div>
							<div class="small-row-contact">
								<span>Neil Donno</span>
								<a href="tel:07860 940440">Tel: 07860 940440</a>
								<a href="mailto:neil@magltd.co.uk">E: neil@magltd.co.uk</a>       
							</div>
							<div class="small-row-contact">
								<span>or call the office on</span>
								<a href="tel:01279 452737"> 01279 452737</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row panel-text-amount padding-side-xl-15">
				<div class="col col-sm-6 col-lg-4">
					<div class="panel-text">
						<h3>
							Temporary water Supplies to enable demolishing works / construction new builds
						</h3>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
						</p>
						<button class="btn btn-sm-padding" type="button" data-toggle="modal" data-target="#modalCenter">
							FIND OUT MORE
						</button>
					</div>
				</div>
				<div class="col col-sm-6 col-lg-4">
					<div class="panel-text">
						<h3>
							Gas isolations and purging
						</h3>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
						</p>
						<button class="btn btn-sm-padding" type="button">
							FIND OUT MORE
						</button>
					</div>
				</div>
				<div class="col col-sm-6 col-lg-4">
					<div class="panel-text">
						<h3>
							Temporary water Supplies to enable demolishing works / construction new builds
						</h3>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
						</p>
						<button class="btn btn-sm-padding" type="button">
							FIND OUT MORE
						</button>
					</div>
				</div>
				<div class="col col-sm-6 col-lg-4">
					<div class="panel-text">
						<h3>
							Temporary water Supplies to enable demolishing works / construction new builds
						</h3>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
						</p>
						<button class="btn btn-sm-padding" type="button">
							FIND OUT MORE
						</button>
					</div>
				</div>
				<div class="col col-sm-6 col-lg-4">
					<div class="panel-text">
						<h3>
							Temporary water Supplies to enable demolishing works / construction new builds
						</h3>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
						</p>
						<button class="btn btn-sm-padding" type="button">
							FIND OUT MORE
						</button>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('inc/works-view.php'); ?>


<?php include('footer.php'); ?>