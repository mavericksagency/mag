
</main>
<footer class="footer">
	<div class="main-footer-row">
		<div class="wrap">
			<div class="row main-footer-row__flex">
				<div class="col col-xl-3 order-top">
					<a href="#" class="footer-logo">
						<img src="img/logo-white.svg" alt="MAG Logo">
					</a>
				</div>
				<div class="col col-md-6 col-xl-5 order-bottom">
					<a href="tel:01279452737" class="promo">
						<span class="promo-title">Have a project?</span>
						<span class="promo-info">Call us now 01279 452737</span>
					</a>
				</div>
				<div class="col col-md-6 col-xl-4 order-middle">
					<nav class="footer-nav">
						<div class="footer-nav-column">
							<h3 class="footer-menu-title">Get to know us</h3>
							<ul class="footer-menu">
								<li><a href="#">About</a></li>
								<li><a href="#">News</a></li>
								<li><a href="#">Case Studies</a></li>
								<li><a href="#">Contact</a></li>
							</ul>
						</div>
						<div class="footer-nav-column">
							<h3 class="footer-menu-title">What we do</h3>
							<ul class="footer-menu">
								<li><a href="#">Mechanical</a></li>
								<li><a href="#">Electrical</a></li>
								<li><a href="#">Environmental</a></li>
								<li><a href="#">Hire Plant</a></li>
							</ul>
						</div>
					</nav>
				</div>
			</div>
		</div>	
	</div>
	<div class="parnter-footer-logo">
		<div class="wrap">
			<div class="footer-slider" id="footer-slider">
				<a href="#" class="footer-slide">
					<img src="img/partners/01.jpg" alt="">
				</a>
				<a href="#" class="footer-slide">
					<img src="img/partners/02.jpg" alt="">
				</a>
				<a href="#" class="footer-slide">
					<img src="img/partners/03.jpg" alt="">
				</a>
				<a href="#" class="footer-slide">
					<img src="img/partners/04.jpg" alt="">
				</a>
				<a href="#" class="footer-slide">
					<img src="img/partners/05.jpg" alt="">
				</a>
				<a href="#" class="footer-slide">
					<img src="img/partners/06.jpg" alt="">
				</a>
				<a href="#" class="footer-slide">
					<img src="img/partners/07.jpg" alt="">
				</a>
				<a href="#" class="footer-slide">
					<img src="img/partners/08.jpg" alt="">
				</a>
				<a href="#" class="footer-slide">
					<img src="img/partners/09.jpg" alt="">
				</a>
				<a href="#" class="footer-slide">
					<img src="img/partners/10.jpg" alt="">
				</a>
			</div>
		</div>
	</div>
	<div class="copyright-row">
		<div class="wrap">
			<div class="copyright">
				<span>©2019 MAG Properties Services Ltd</span>
				<span>Website Design & Build: Brandality</span>
			</div>
			<ul class="footer-privacy-menu">
				<li><a href="#">Cookie Policy</a></li>
				<li><a href="#">Terms & Conditions</a></li>
			</ul>
		</div>
	</div>
</footer>

<?php include('inc/modal.php'); ?>
<?php include('inc/loader.php'); ?>
<?php include('inc/dev-menu.php'); ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="bootstrap/bootstrap.min.js"></script>
<script src="slick/slick.js"></script>
<script src="js/main.js"></script>
</body>
</html>