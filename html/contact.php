<?php include('header.php'); ?>
<?php $title = 'Get in touch'; include('inc/title.php'); ?>
	<section class="section section-content section-negative-margin anim-block transformY-top">
		<div class="wrap">
			<div class="row">
				<div class="col col-sm-6 col-lg-5">
					<img src="img/content-img/contact-img.jpg" alt="">
				</div>
				<div class="col col-sm-6 col-lg-7">
					<div class="adress">
						MAG Properties Services Ltd<br>
						Unit 18 Burnt Mill,<br>
						Harlow,<br>
						Essex,<br>
						CM20 2HS
					</div>
					<a href="tel:01279452737" class="contacts-phone">
						Main Office: 01279 452737
					</a>
				</div>
			</div>
			<div class="adress-grid">
				<div class="row">
					<div class="col col-sm-6 col-lg-3">
						<div class="adress-col">
							<h3>
								Mechanical
							</h3>
							<div class="person-adress">
								<h4>
									James Gull    
								</h4>
								<a href="tel:07831 331 269">
									Tel: 07831 331 269 
								</a>
								<a href="mailto:james@magltd.co.uk">Email: james@magltd.co.uk </a>
							</div>
							<div class="person-adress">
								<h4>
									Neil Donno
								</h4>
								<a href="tel:07860 940 440">
									Tel: 07860 940 440
								</a>
								<a href="mailto:neil@magltd.co.uk">Email: neil@magltd.co.uk </a>
							</div>
						</div>
					</div>
					<div class="col col-sm-6 col-lg-3">
						<div class="adress-col">
							<h3>
								Electrical
							</h3>
							<div class="person-adress">
								<h4>
									James Gull    
								</h4>
								<a href="tel:07831 331 269">
									Tel: 07831 331 269 
								</a>
								<a href="mailto:james@magltd.co.uk">Email: james@magltd.co.uk </a>
							</div>
							<div class="person-adress">
								<h4>
									Neil Donno
								</h4>
								<a href="tel:07860 940 440">
									Tel: 07860 940 440
								</a>
								<a href="mailto:neil@magltd.co.uk">Email: neil@magltd.co.uk </a>
							</div>
							<div class="person-adress">
								<h4>
									Neil Donno
								</h4>
								<a href="tel:07860 940 440">
									Tel: 07860 940 440
								</a>
								<a href="mailto:neil@magltd.co.uk">Email: neil@magltd.co.uk </a>
							</div>
						</div>
					</div>
					<div class="col col-sm-6 col-lg-3">
						<div class="adress-col">
							<h3>
								Environmental
							</h3>
							<div class="person-adress">
								<h4>
									Mark Gull    
								</h4>
								<a href="tel:07831 331 269">
									Tel: 07831 331 269 
								</a>
								<a href="mailto:mark@magltd.co.uk">Email: mark@magltd.co.uk </a>
							</div>
							<div class="person-adress">
								<h4>
									Neil Donno
								</h4>
								<a href="tel:07860 940 440">
									Tel: 07860 940 440
								</a>
								<a href="mailto:neil@magltd.co.uk">Email: neil@magltd.co.uk </a>
							</div>
							<div class="person-adress">
								<h4>
									Neil Donno
								</h4>
								<a href="tel:07860 940 440">
									Tel: 07860 940 440
								</a>
								<a href="mailto:neil@magltd.co.uk">Email: neil@magltd.co.uk </a>
							</div>
						</div>
					</div>
					<div class="col col-sm-6 col-lg-3">
						<div class="adress-col">
							<h3>
								Hire Plant
							</h3>
							<div class="person-adress">
								<h4>
									Mark Gull    
								</h4>
								<a href="tel:07831 331 269">
									Tel: 07831 331 269 
								</a>
								<a href="mailto:mark@magltd.co.uk">Email: mark@magltd.co.uk </a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section section-contact">
		<div class="wrap-sm">
			<h2>
				Fill in the form and we will contact you
			</h2>
			<form class="contact-form">
				<div class="row-label">
					<label class="label">
						<span>Name</span>
						<input type="text" name="name" required>
					</label>
					<label class="label">
						<span></span>
						<input type="text" name="surname" required>
					</label>
				</div>
				<label class="label">
					<span>Email</span>
					<input type="email" name="email" required>
				</label>
				<label class="label">
					<span>Phone</span>
					<input type="number" name="phone" required>
				</label>
				<label class="label">
					<span>Company</span>
					<input type="text" name="company" required>
				</label>
				<label class="label">
					<span>Message</span>
					<textarea name="message" rows="6" required></textarea>
				</label>
				<button type="submit" class="btn-submit dark">
					Send
				</button>
			</form>
		</div>
	</section>

<?php include('footer.php'); ?>